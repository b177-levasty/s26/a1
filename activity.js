let http = require("http");

const port = 3000;

http.createServer(function (request, response) {
	if(request.url == '/login'){
		response.writeHead(200, {'content-type': 'text/plain'})
		response.end(`Welcome to the log-in page`);
	}
	else{
		response.writeHead(404, {'content-type': 'text/plain'})
		response.end(`I'm sorry, the page that you're looking for cannot be found`);
	}
}).listen(3000);

console.log(`Server running at localhost:3000`);